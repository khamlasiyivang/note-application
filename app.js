
/**
 * Library
 * */
const validator = require("validator");
const chalk = require("chalk")

/**
 * component
 * **/
const getNotes = require('./notes.js')


/**
 * function getNotes
 */
const msg = getNotes()


/**
 * function */

const greenMsg = chalk.green.inverse.bold.underline("Success");

console.log(msg);
console.log(validator.isURL("www.mead.io"));
console.log(greenMsg)